# Stage 1: Base image
FROM alpine:3.14 as base
RUN apk add --no-cache curl tar jq

# Stage 2: Install kubectl
FROM base as kubectl
RUN KUBECTL_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" && \
    chmod +x kubectl && \
    mv kubectl /usr/local/bin/

# Stage 3: Install kustomize
FROM base as kustomize
RUN KUSTOMIZE_VERSION=$(curl -s https://api.github.com/repos/kubernetes-sigs/kustomize/releases/latest | jq -r .tag_name | cut -d \/ -f 2) && \
    curl -LO "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz" && \
    tar -xzf kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
    mv kustomize /usr/local/bin/

# Stage 4: Install kubeval
FROM base as kubeval
RUN KUBEVAL_VERSION=$(curl -s https://api.github.com/repos/instrumenta/kubeval/releases/latest | jq -r .tag_name) && \
    curl -LO "https://github.com/instrumenta/kubeval/releases/download/${KUBEVAL_VERSION}/kubeval-linux-amd64.tar.gz" && \
    tar -xzf kubeval-linux-amd64.tar.gz && \
    mv kubeval /usr/local/bin/

# Stage 5: Install kube-score
FROM base as kubescore
RUN KUBESCORE_VERSION=$(curl -s https://api.github.com/repos/zegl/kube-score/releases/latest | jq -r .tag_name | tr -d v) && \
    curl -LO "https://github.com/zegl/kube-score/releases/download/v${KUBESCORE_VERSION}/kube-score_${KUBESCORE_VERSION}_linux_amd64.tar.gz" && \
    tar -xzf kube-score_${KUBESCORE_VERSION}_linux_amd64.tar.gz && \
    mv kube-score /usr/local/bin/

FROM base as helm
RUN HELM_VERSION=$(curl -s https://api.github.com/repos/helm/helm/releases/latest | jq -r .tag_name) && \
    curl -LO "https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz" && \
    tar -xzf helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/

# Final stage: Copy all binaries to final image
FROM alpine:3.14
COPY --from=kubectl /usr/local/bin/kubectl /usr/local/bin/
COPY --from=kustomize /usr/local/bin/kustomize /usr/local/bin/
COPY --from=kubeval /usr/local/bin/kubeval /usr/local/bin/
COPY --from=kubescore /usr/local/bin/kube-score /usr/local/bin/
COPY --from=helm /usr/local/bin/helm /usr/local/bin/
CMD ["/bin/sh"]
