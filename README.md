[![CC BY-ND 4.0][cc-by-nd-shield]][cc-by-nd]

This work is licensed under a [Creative Commons Attribution-NoDerivatives 4.0 International License][cc-by-nd].

[cc-by-nd]: http://creativecommons.org/licenses/by-nd/4.0/
[cc-by-nd-shield]: https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge

# KubeBox Docker Image Usage

The Docker image built from the provided Dockerfile contains several tools useful for interacting with Kubernetes: `kubectl`, `kustomize`, `kubeval`, and `kube-score`. Here's how you can use this Docker image and its tools.

## Running the Docker Image

To run the Docker image, you can use the following command:

```bash
docker run -it --name kubebox <image-name>
```

Replace `<image-name>` with the name of the Docker image you've built.

## Using the Tools

### kubectl

`kubectl` is a command line tool for controlling Kubernetes clusters. Here's an example of how you can use `kubectl` to get the nodes of your Kubernetes cluster:

```bash
kubectl get nodes
```

### kustomize

`kustomize` is a standalone tool to customize Kubernetes objects through a kustomization file. Here's an example of how you can use `kustomize` to build a set of Kubernetes objects:

```bash
kustomize build <kustomization_directory>
```

Replace `<kustomization_directory>` with the path to the directory containing your kustomization file.

### kubeval

`kubeval` is a tool for validating a Kubernetes YAML or JSON configuration file. It can be used as follows:

```bash
kubeval my-deployment.yaml
```

Replace `my-deployment.yaml` with the path to your Kubernetes configuration file.

### kube-score

`kube-score` is a tool that performs static code analysis of your Kubernetes object definitions. The goal of kube-score is to identify security misconfigurations. Here's how you can use it:

```bash
kube-score score my-deployment.yaml
```

Replace `my-deployment.yaml` with the path to your Kubernetes configuration file.

# Using the Docker Image in GitLab CI/CD

You can use the Docker image in your GitLab CI/CD jobs by specifying it as the image to be used in your `.gitlab-ci.yml` file. Here's an example:

```yaml
stages:
  - validate

validate:
  stage: validate
  image: 
    name: <image-name>
    entrypoint: [""]
  script:
    - kubeval my-deployment.yaml
    - kube-score score my-deployment.yaml
```

Replace `<image-name>` with the name of your Docker image. This GitLab CI/CD job will validate your Kubernetes configuration file using both `kubeval` and `kube-score`.

Remember to replace `<image-name>` and other placeholders with your actual values. Enjoy using KubeBox! 🚀
